#!/usr/bin/env python3

import math
import argparse
import os
import sys

from bs4 import BeautifulSoup
import bs4

from downloader import Downloader

scores = {}


def score_a_tag(tag, depth):
    """
    Takes a BeautifulSoup tag and calculate it's score.
    If it contains a lot of text then it optimize,
    else pessimize.

    Returns: tag score including all descendants
    """

    if isinstance(tag, bs4.element.NavigableString):
        return 0

    banned_tags = ['script', 'head', 'style',  'meta', 'link', 'br', ]
    if tag.name in banned_tags:
        return 0

    chars = 1
    tags = 1
    hyperchars = 1
    text = ' '.join(tag.stripped_strings)
    chars += len(text)

    def is_cool(x):
        if isinstance(tag, bs4.element.NavigableString):
            return False
        if tag.name in banned_tags:
            return False
        return True

    tags += len(list(filter(lambda x: is_cool(x), tag.descendants)))

    banned_tags_count = 0
    for bt in banned_tags:
        banned_tags_count += len(tag.find_all(bt))

    tags -= banned_tags_count

    for a_link_tag in tag.find_all('a'):
        hyperchars += len(' '.join(a_link_tag.stripped_strings))

    score = (chars / tags) * math.log2(chars / hyperchars)  # * len(list(text.split(',')))
    tag['score'] = score
    tag['hyperchars'] = hyperchars
    tag['chars'] = chars
    tag['tags'] = tags
    return score


def traverse_html_tree(tag, depth=0):
    summ = 0
    for child in tag.children:
        if isinstance(child, bs4.element.NavigableString):
            continue
        summ += score_a_tag(child, depth)
        traverse_html_tree(child, depth + 1)
    scores[summ] = tag
    tag['weight'] = summ


def parse_page(page_content):
    """
    This page set scores to page and save info about relevance
    for future sorting purposes.

    Returns: most scored BeautifulSoup tag structure
    """
    scores.clear()
    root = BeautifulSoup(page_content, 'html.parser')

    traverse_html_tree(root)
    open('output.html', 'w').write(root.prettify())

    res = scores[max(scores.keys())]
    return res


def limit_string_to_80_chars(s):
    """
    Returns: string with \n chars every 80 chars.
    """
    result = ""
    paragraphs = s.split('\n')
    for paragraph in paragraphs:
        words = paragraph.split(' ')
        currentLength = 0
        for word in words:
            if len(word) + currentLength > 80:
                result += "\n" + word
                currentLength = 0
            else:
                if currentLength == 0:
                    result += word
                else:
                    result += " " + word
            currentLength += len(word) + 1
        result += "\n"
    return result


def recursive_textuator(tag):
    result = ""

    if isinstance(tag, bs4.element.NavigableString):
        return tag.string

    headers_tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']

    for child in tag.children:
        result += recursive_textuator(child)
        if child.name == 'a':
            try:
                result += " [{}]".format(child.attrs['href'])
            except:
                pass
        if child.name in headers_tags:
            result += "\n"

    return result


def format_text(tag):
    """
    Gets BeautifulSoup tag then
    Format text with 80 chars line-width and
    incapsulate links into text within square bracets []

    Returns: formatted text
    """
    text = limit_string_to_80_chars(recursive_textuator(tag))
    return text


def load_links(filename):
    return open(filename).readlines()


def main(filename, single_url):
    if filename is not None:
        d = Downloader()
        for link in load_links(filename):
            rawHTML = d.download_page(link)
            most_relevant_tag = parse_page(rawHTML)
            textBlock = format_text(most_relevant_tag)
            open(d.create_cache_dir_for_url(link) + "/index.txt", 'w')\
                .write(textBlock)

    if single_url is not None:
        d = Downloader()
        if os.path.exists(single_url):
            rawHTML = open(single_url).read()
        else:
            rawHTML = d.download_page(single_url)
        most_relevant_tag = parse_page(rawHTML)
        textBlock = format_text(most_relevant_tag)
        d.save_to_disc(single_url, rawHTML)
        open(d.create_cache_dir_for_url(single_url) + "/index.txt", 'w')\
            .write(textBlock)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=True, description="""This program download page from URL,\
            then save it to folder with line-width 80 chars""")
    parser.add_argument('-i', action='store', dest='filename',
                        help='file with links to parse')
    parser.add_argument('-u', action='store', dest='URL',
                        help="URL to download and parse")
    args = parser.parse_args()
    if len(sys.argv) == 1:
        parser.print_help()

    main(args.filename, args.URL)
