import requests
import os


class Downloader:
    rootFolderName = 'pages'
    savePages = True

    def __init__(self):
        pass

    def create_cache_dir_for_url(self, url):
        clean_url = str(url)\
            .replace('https:', '')\
            .replace('\n', '')\
            .replace('http:', '')
        dirname = './' + self.rootFolderName + '/' + clean_url
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        return dirname

    def save_to_disc(self, url, content):
        """
        Save to folder, rename by url,
        """
        dirname = self.create_cache_dir_for_url(url)
        open(dirname + '/page.html', 'w').write(str(content))

    def download_page(self, url):
        print(url)
        response = requests.get(url, headers={
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:53.0)\
                    Gecko/20100101 Firefox/53.0',
            })
        try:
            content = response.content.decode('utf-8')
        except:
            content = response.content
        content = "\n".join(content.splitlines())
        self.save_to_disc(url, content)
        return content
